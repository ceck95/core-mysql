module.exports = {
  BaseAdapter: require('./src/adapter/base-adapter'),
  BaseModel: require('./src/model/base-model')
};