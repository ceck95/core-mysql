const Hoek = require('hoek');
const helpers = require('./helpers');
const BPromise = require('bluebird');
const Pool = require('../model/pool');
const config = require('config');
const log = require('core-express').Log;
const helpersCore = require('helpers');

class BaseAdapter {
  constructor(modelClass) {
    this.model = modelClass;
  }

  static async instance(instanceName, nameModel, adapterClass) {
    const classCurrent = require(`${process.cwd()}/models/${nameModel}`),
      infoModel = new classCurrent(),
      modelClass = await classCurrent.instance(
        infoModel.tableName,
        classCurrent
      );
    Hoek.assert(
      classCurrent,
      `${instanceName} cannot found model ${nameModel}`
    );
    return new adapterClass(modelClass);
  }

  get logName() {
    return 'MySQL';
  }

  get log() {
    if (!this._log)
      this._log = log.child({
        namespace: this.logName,
        adapter: this.constructor.name
      });

    return this._log;
  }

  queryBase(sql, args = null, opts = {}) {
    return new BPromise((resolve, reject) => {
      this.log.debug('------------------------------');
      this.log.debug('Query begin:');
      this.log.debug(`Sql query: ${sql}`);
      return Pool.query(sql, args, (error, results) => {
        if (error) {
          this.log.error(error);
          return reject(error);
        }

        const parseObj = obj => {
          const keys = Object.keys(obj),
            result = {};
          if (keys.length === 0) return obj;
          keys.forEach(e => {
            result[helpers.toCamelCase(e)] = obj[e];
          });
          return result;
        };
        if (Array.isArray(results)) {
          results = results.map(e => {
            return parseObj(e);
          });
          this.log.debug(results);
          return resolve(results);
        }
        results = parseObj(results);
        this.log.debug(results);
        return resolve();
      });
    });
  }

  query(sql, args, opts) {
    return new BPromise((resolve, reject) => {
      this.log.debug('------------------------------');
      this.log.debug('Query begin:');
      this.log.debug(`Sql query: ${sql}`);
      if (args && args.length > 0) this.log.debug(`Args: ${args}`);

      return Pool.query(sql, args, (error, results) => {
        if (error) {
          this.log.error(error);
          return reject(error);
        }

        let listTable = [this.model.tableAlias];

        if (opts) {
          if (Array.isArray(opts.includes) && opts.includes.length > 0) {
            opts.includes.forEach(e => {
              if (e.storeModel)
                listTable.push(e.tableAlias || e.storeModel.model.tableAlias);

              if (e.joinMany && e.joinMany.length > 0)
                e.joinMany.forEach(join => {
                  listTable.push(
                    join.tableAlias || join.storeModel.model.tableAlias
                  );
                });
            });
          }

          this.log.debug(`Tables: ${listTable.join(', ')}`);

          if (opts.insert) {
            this.log.debug('Data: ');
            this.log.debug(opts.dataInsert);
            this.log.debug(`Insert ${results.affectedRows} rows successfully`);
          } else if (opts.delete)
            this.log.debug(`Delete ${results.affectedRows} rows successfully`);
          else if (opts.update) {
            this.log.debug('Data: ');
            this.log.debug(opts.dataUpdate);
            this.log.debug(`Update ${results.affectedRows} rows successfully`);
          } else this.log.debug(`Rows received: ${results.length}`);
        } else this.log.debug(`Tables: ${listTable.join(', ')}`);

        return resolve(results);
      });
    });
  }

  queryResponse(sql, args, opts) {
    return new BPromise((resolve, reject) => {
      opts = opts || {};
      return this.query(sql, args, opts).then(results => {
        if (Array.isArray(opts.includes) && opts.includes.length > 0) {
          let dataRelationOneMany = [];
          opts.includes.forEach(e => {
            if (e.relationType && e.relationType === 'oneMany')
              dataRelationOneMany.push(e);
          });
          if (dataRelationOneMany.length > 0) {
            return this.response(results, opts).then(results => {
              let isDataResponseObj = false;
              if (!Array.isArray(results)) {
                results = [results];
                isDataResponseObj = true;
              }

              const getDataJoin = index => {
                if (index === dataRelationOneMany.length) {
                  if (isDataResponseObj) {
                    if (results.length > 0) return BPromise.resolve(results[0]);
                    return BPromise.resolve({});
                  }
                  return BPromise.resolve(results);
                } else {
                  const dataForKey = indexR => {
                    if (indexR === results.length) {
                      return BPromise.resolve(results);
                    } else {
                      const current = dataRelationOneMany[index];
                      let dataFilter = {
                        [current.columns[0]]:
                          results[indexR][current.columns[1]]
                      };
                      if (current.where)
                        dataFilter = Object.assign(dataFilter, current.where);

                      return current.storeModel
                        .filter(dataFilter, {
                          getMany: true
                        })
                        .then(data => {
                          const modelCurrent = current.storeModel.model,
                            keyData = helpers.toCamelCase(modelCurrent.table);
                          results[indexR][keyData] = data;
                          return dataForKey(++indexR);
                        })
                        .catch(err => {
                          return BPromise.reject(err);
                        });
                    }
                  };
                  return dataForKey(0)
                    .then(data => {
                      results = data;
                      return getDataJoin(++index);
                    })
                    .catch(err => {
                      return BPromise.reject(err);
                    });
                }
              };
              return getDataJoin(0)
                .then(final => {
                  return resolve(final);
                })
                .catch(err => {
                  return reject(err);
                });
            });
          } else {
            return resolve(this.response(results, opts));
          }
        } else {
          return resolve(this.response(results, opts));
        }
      });
    });
  }

  response(results, opts) {
    let getKey = (obj, model, opts) => {
      let tableAlias = opts
          ? opts.tableAlias || model.tableAlias
          : model.tableAlias,
        resp = {};
      Object.keys(obj)
        .filter(a => {
          if (a.indexOf(tableAlias) < 0) {
            return false;
          }
          return true;
        })
        .map(key => {
          if (
            obj[key] === null ||
            obj[key] === '' ||
            typeof obj[key] === 'undefined'
          ) {
            obj[key] = null;
          }
          return (resp[helpers.toCamelCase(key.replace(`${tableAlias}_`, ''))] =
            obj[key]);
        });
      resp = helpers.responseData(resp, model);
      return resp;
    };

    if (results.length ? results.length > 0 : false) {
      let resp = [];
      results.forEach(e => {
        let currentResp = getKey(e, this.model);
        const addValueToResponse = (src, storeModel, opts) => {
          let modelJoin = storeModel.model.tableAlias,
            tableAliasJoin = config.mysql.prefix
              ? modelJoin.replace(config.mysql.prefix, '')
              : modelJoin;
          tableAliasJoin =
            opts.tableAlias || helpers.toCamelCase(tableAliasJoin);
          currentResp[tableAliasJoin] = getKey(src, storeModel.model, opts);
        };
        if (opts.includes ? opts.includes.length > 0 : false)
          opts.includes.forEach(a => {
            if (this.checkRelationType(a)) {
              if (a.storeModel)
                addValueToResponse(e, a.storeModel, {
                  tableAlias: a.tableAlias
                });

              if (a.joinMany && a.joinMany.length > 0)
                a.joinMany.forEach(join => {
                  addValueToResponse(e, join.storeModel, {
                    tableAlias: join.tableAlias
                  });
                });
            }
          });
        resp.push(currentResp);
      });

      if (resp.length === 1) {
        if (opts.getMany || (opts.limit && opts.limit > 1))
          return BPromise.resolve(resp);
        return BPromise.resolve(resp[0]);
      }
      return BPromise.resolve(resp);
    }

    if (opts.checkNull) {
      const errorNotFound = {
        code: '202',
        source: 'record not found'
      };
      this.log.debug(errorNotFound);
      return BPromise.reject(errorNotFound);
    }

    if (opts.getMany || (opts.limit && opts.limit > 1))
      return BPromise.resolve([]);

    return BPromise.resolve({});
  }

  get columnNames() {
    return helpers.extractColumnName(this.model);
  }

  checkRelationType(join) {
    if (join && Array.isArray(join.joinMany) && join.joinMany.length > 0) {
      let ignoreJoin = true;
      join.joinMany.forEach(e => {
        if (e.relationType === 'oneMany') {
          Hoek.assert(false, 'Join many cannot exist key "relationType"');
          ignoreJoin = true;
        }
      });
      return ignoreJoin;
    }
    if (join.relationType && join.relationType === 'oneMany') return false;
    return true;
  }

  filterCommon(params, selectOptions, opts) {
    opts = opts || {};
    params = params || {};

    let sql,
      args = [],
      model = this.model,
      columns = [],
      tableName = `${model.tableName} ${model.tableAlias}`,
      filterQuery = '',
      sqlRelationship;

    const parseOrWhere = (dataWhere, args, argsInit, filterQueryInit) => {
      if (dataWhere.ors.length > 0) {
        dataWhere.ors.forEach((e, i) => {
          let filterQueryCurrent;
          if (i > 0)
            e.where = `${filterQueryInit.replace('WHERE', '')} ${
              filterQueryInit !== '' ? 'AND' : ''
            } ${e.where}`;
          filterQuery += `${i === 0 ? (filterQuery !== '' ? 'AND' : '') : ''} ${
            e.where
          } ${i === 0 ? ' OR' : ''}`;
          if (i === 0) {
            args.push(e.arg);
          } else {
            args = args.concat(argsInit.concat(e.arg));
          }
        });
      }
      return args;
    };

    if (opts && opts.extendArgs) args = opts.extendArgs;

    if (
      selectOptions
        ? selectOptions.includes ? selectOptions.includes.length > 0 : false
        : false
    ) {
      opts.functionCheckRelation = {
        checkRelationType: this.checkRelationType
      };
      let relation = helpers.sqlRelation(model, selectOptions, opts),
        modelExtractColumns = [model],
        sqlJoinWhere = [],
        dataWhere = helpers.filterParams(params, model, selectOptions);

      const checkTableAlias = current => {
        if (current.tableAlias) {
          modelExtractColumns.push({
            modelClass: current.storeModel.model,
            tableAlias: current.tableAlias
          });
        } else {
          modelExtractColumns.push(current.storeModel.model);
        }
      };

      selectOptions.includes.forEach(e => {
        if (this.checkRelationType(e)) {
          if (e.storeModel) checkTableAlias(e);

          if (e.joinMany && e.joinMany.length > 0)
            e.joinMany.forEach(join => {
              checkTableAlias(join);
            });
        }
      });
      let argsJoinWhere = [];
      relation.sqlJoinWhere.forEach(e => {
        argsJoinWhere = argsJoinWhere.concat(e.args);
        sqlJoinWhere = sqlJoinWhere.concat(e.where);
      });

      columns =
        opts.select ||
        helpers.extractColumnNameString(modelExtractColumns, opts).join(', ');

      sqlRelationship = `${relation.sqlJoin.join(' ')}`;

      if (dataWhere.where.length > 0 || sqlJoinWhere.length > 0)
        filterQuery = `${
          opts.extendWhere ? 'AND' : 'WHERE'
        } ${helpers.extractSqlWhere(
          dataWhere.where,
          sqlJoinWhere
        )} ${helpers.extractSqlWhere(sqlJoinWhere)}`;
      args = args.concat(dataWhere.args.concat(argsJoinWhere));
      const argsInit = helpersCore.Json.cloneDeep(args),
        filterQueryInit = filterQuery;
      args = parseOrWhere(dataWhere, args, argsInit, filterQueryInit);
    } else {
      columns =
        opts.select || helpers.extractColumnNameString(model, opts).join(', ');
      const dataWhere = helpers.filterParams(params, model, opts);
      filterQuery = `${
        dataWhere.where.length > 0
          ? ` ${opts.extendWhere ? 'AND' : 'WHERE'} ${dataWhere.where.join(
              ' AND '
            )}`
          : ''
      }`;
      args = args.concat(dataWhere.args);
      const argsInit = helpersCore.Json.cloneDeep(args);
      args = parseOrWhere(dataWhere, args, argsInit);
    }

    if (opts && opts.extendSelect) {
      columns += `${columns ? ',' : ''}${opts.extendSelect.join(',')}`;
    }

    sql = `SELECT ${columns} FROM ${tableName}`;
    sql = `${sql} ${sqlRelationship || ''} `;

    if (opts && opts.extendWhere)
      sql = `${sql} WHERE ${opts.extendWhere.join(' AND ')}`;
    sql = `${sql} ${filterQuery}${helpers.sqlOrder(
      model,
      selectOptions,
      opts
    )}`;

    return {
      sql: sql,
      args: args,
      filter: filterQuery,
      sqlRelationship: sqlRelationship || ''
    };
  }

  filter(params, selectOptions, opts) {
    let filterCommon = this.filterCommon(params, selectOptions, opts),
      sql = `${filterCommon.sql} ${helpers.sqlLimit(selectOptions, opts)}`,
      args = filterCommon.args;
    if (opts && opts.select)
      return this.query(sql, args, opts).then(result => {
        return BPromise.resolve(result[0]);
      });

    return this.queryResponse(sql, args, selectOptions).then(results => {
      return BPromise.resolve(results);
    });
  }

  count(params, selectOptions, opts) {
    opts = opts || {};
    opts.select = 'COUNT(*) AS count';
    const filterCommon = this.filterCommon(params, selectOptions, opts),
      sqlCount = filterCommon.sql;
    return this.query(sqlCount, filterCommon.args, selectOptions)
      .then(rows => {
        return BPromise.resolve(rows[0]);
      })
      .catch(err => {
        return BPromise.reject(err);
      });
  }

  maxId(params, selectOptions, opts) {
    opts = opts || {};
    const primary = this.model.primaryKey;
    opts.select = `max(${primary}) as maxId`;
    const filterCommon = this.filterCommon(params, selectOptions, opts),
      sqlMaxId = filterCommon.sql;
    return this.query(sqlMaxId, filterCommon.args, selectOptions)
      .then(rows => {
        return BPromise.resolve(rows[0].maxId);
      })
      .catch(err => {
        return BPromise.reject(err);
      });
  }

  filterPagination(params, selectOptions, opts) {
    Hoek.assert(selectOptions.paging, 'Cannot empty params paging');
    Hoek.assert(
      !selectOptions.limit,
      'Filter pagination cannot exist param limit'
    );
    opts = opts || {};
    let paging = selectOptions.paging,
      optsCount = {
        select: 'COUNT(*) AS total'
      };

    optsCount = Object.assign(optsCount, opts);
    delete optsCount.extendSelect;
    delete optsCount.extendOrder;
    delete optsCount.extendArgs;
    if (optsCount.extendCountArgs) optsCount.extendArgs = opts.extendCountArgs;

    let filterCommon = this.filterCommon(params, selectOptions, optsCount),
      sqlCount = filterCommon.sql;

    return this.query(sqlCount, filterCommon.args, selectOptions).then(
      countRows => {
        let totalRows = countRows[0].total,
          pageSize = paging.pageSize > 0 ? paging.pageSize : totalRows,
          totalPages =
            totalRows % pageSize === 0
              ? totalRows / pageSize
              : Math.floor(totalRows / pageSize) + 1;

        let resp = {
            meta: {
              pageSize: pageSize,
              pageNumber: paging.pageNumber,
              totalPages: totalPages,
              total: totalRows
            }
          },
          sqlPagination = helpers.sqlPagination(selectOptions.paging);

        if (sqlPagination.pageIndex > totalRows) {
          resp.data = [];
          resp.meta.count = 0;
          return BPromise.resolve(resp);
        }

        delete opts.select;
        filterCommon = this.filterCommon(params, selectOptions, opts);
        let sqlFilterPagination = `${filterCommon.sql} ${sqlPagination.sql}`,
          argsFilterPagination = filterCommon.args;
        return this.queryResponse(
          sqlFilterPagination,
          argsFilterPagination,
          selectOptions
        ).then(results => {
          if (!results.length)
            if (Object.keys(results).length === 0) results = [];
            else results = [results];

          resp.meta.count = results.length;
          resp.data = results;

          return BPromise.resolve(resp);
        });
      }
    );
  }

  commonInsert(dataRowInsert) {
    const model = this.model,
      columnModel = Object.keys(model);
    let columnInsert = [],
      dataInsert = [];

    for (let row of Object.keys(dataRowInsert)) {
      if (columnModel.indexOf(row) < 0) {
        Hoek.assert(
          false,
          `Insert table ${model.tableName} ${
            model.tableAlias
          } not found column ${row}`
        );
        break;
      } else {
        columnInsert.push(helpers.toSnakeCase(row));
        dataInsert.push(dataRowInsert[row]);
      }
    }

    return {
      columnInsert: columnInsert,
      dataInsert: dataInsert
    };
  }

  insert(dataRowInsert, opts) {
    Hoek.assert(typeof dataRowInsert === 'object', 'Data insert must object');
    Hoek.assert(
      Object.keys(dataRowInsert).length > 0,
      'Data insert cannot null'
    );
    opts = opts || {};
    const model = this.model,
      columnModel = Object.keys(model),
      optFormData = {
        insert: true
      };
    let columnInsert, dataInsert;

    dataRowInsert = Array.isArray(dataRowInsert)
      ? dataRowInsert.map(e => {
          return helpers.formData(e, model, optFormData);
        })
      : helpers.formData(dataRowInsert, model, optFormData);

    if (!Array.isArray(dataRowInsert)) {
      const insertCommon = this.commonInsert(dataRowInsert);
      columnInsert = insertCommon.columnInsert;
      dataInsert = insertCommon.dataInsert;
    } else {
      let insertMany = [];
      for (let e of dataRowInsert) {
        if (Object.keys(e).length !== Object.keys(dataRowInsert[0]).length) {
          Hoek.assert(false, 'Insert many all row have columns equal');
        }
      }

      dataRowInsert.forEach(e => {
        insertMany.push(this.commonInsert(e));
      });

      columnInsert = insertMany[0].columnInsert;
      dataInsert = [];
      insertMany.forEach(e => {
        dataInsert.push(e.dataInsert);
      });
      dataInsert = [dataInsert];
    }
    let valueSyntax;
    if (!Array.isArray(dataRowInsert)) {
      valueSyntax = ` VALUES (${columnInsert
        .map(() => {
          return '?';
        })
        .join(',')})`;
    } else {
      valueSyntax = ' VALUES ?';
    }
    const sqlInsert = `INSERT INTO ${model.tableName} (${columnInsert.join(
      ','
    )}) ${valueSyntax}`;

    opts = Object.assign(opts, {
      insert: true,
      dataInsert: dataRowInsert
    });

    return this.query(sqlInsert, dataInsert, opts)
      .then(result => {
        if (opts.returnData) {
          if (!Array.isArray(dataRowInsert)) {
            return this.filter({
              [columnModel[0]]: result.insertId
            })
              .then(result => {
                return BPromise.resolve(result);
              })
              .catch(() => {
                this.log.error(
                  ` Insert sql Error: ${model.tableName} not exist primary key `
                );
                return BPromise.resolve(result.insertId);
              });
          } else {
            return BPromise.resolve(
              dataRowInsert.map(e => {
                return helpers.responseData(e, model);
              })
            );
          }
        }
        return BPromise.resolve(result.insertId);
      })
      .catch(err => {
        if (err.code === 'ER_DUP_ENTRY') {
          return BPromise.reject({
            code: '203',
            source: 'core sql'
          });
        }
        return BPromise.reject(err);
      });
  }

  delete(params, selectOptions, opts) {
    let filterCommon = this.filterCommon(params, selectOptions, opts),
      model = this.model,
      tableName = `${model.tableName}`,
      sql = `DELETE FROM ${tableName} ${filterCommon.filter}`,
      args = filterCommon.args;

    return this.query(sql, args, {
      delete: true
    }).then(() => {
      return BPromise.resolve(true);
    });
  }

  update(data, params, selectOptions, opts) {
    const model = this.model,
      columnModel = Object.keys(model),
      dataKeys = Object.keys(data),
      filterCommon = this.filterCommon(params, selectOptions, opts);

    let columnUpdate = [],
      dataUpdate = [];

    data = helpers.formData(data, model, {
      update: true
    });
    dataKeys.forEach(e => {
      if (columnModel.indexOf(e) < 0) {
        Hoek.assert(
          false,
          `Update key ${e} not found in table ${model.tableName}`
        );
      }
      columnUpdate.push(`${helpers.toSnakeCase(e)} = ?`);
      dataUpdate.push(data[e]);
    });

    let sql = `UPDATE ${model.tableName} ${
        filterCommon.sqlRelationship
      } SET ${columnUpdate.join(',')} ${filterCommon.filter}`,
      args = dataUpdate.concat(filterCommon.args);
    return this.query(sql, args, {
      update: true,
      dataUpdate: data
    }).then(() => {
      if (opts && opts.returnData) {
        return this.filter(params, selectOptions)
          .then(result => {
            return BPromise.resolve(result);
          })
          .catch(err => {
            return BPromise.reject(err);
          });
      }
      return BPromise.resolve(true);
    });
  }
}

module.exports = BaseAdapter;
