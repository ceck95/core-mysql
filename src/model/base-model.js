const config = require('config');
const Hoek = require('hoek');
const pool = require('./pool');
const helperAdapter = require('../adapter/helpers');
const log = require('core-express').Log;
const BPromise = require('bluebird');

class BaseModel {

  constructor(data) {
    if (data)
      Object.keys(data).forEach(e => {
        this[e] = data[e];
      });
  }

  static instance(tableName, classCurrent) {
    return new BPromise((resolve, reject) => {
      const typeNull = ['int', 'bigint', 'decimal', 'double', 'float', 'mediumint', 'real', 'smallint', 'tinyint'];
      return pool.query('SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = ?', [tableName], (error, results, field) => {
        let data = {};
        const initValueColumn = (item) => {
          let currentType = null;
          if (typeNull.indexOf(item.DATA_TYPE.toLowerCase()) < 0) {
            currentType = '';
          }
          data[helperAdapter.toCamelCase(item.COLUMN_NAME.toLowerCase())] = currentType;
        };

        let checkPrimaryKey = false;

        results.every((e, i) => {
          if (e.COLUMN_KEY === 'PRI') {
            initValueColumn(e);
            checkPrimaryKey = true;
            results.splice(i, 1);
            return false;
          }
          return true;
        });

        if (!checkPrimaryKey) {
          log.error(`${tableName} not found primary key`);
        }

        results.forEach(e => {
          initValueColumn(e);
        });
        Hoek.assert(Object.keys(data).length !== results.length, `[Base model] Generate columns table ${this.tableName} catch error`);
        log.info(`Table name ${tableName}`);
        log.info(data);
        return resolve(new classCurrent(data));
      });
    });
  }

  get tableName() {
    Hoek.assert(this.table, '[Model Base] function table cannot null');
    return config.mysql ? `${config.mysql.prefix}${this.table}` : this.table;
  }

  get primaryKey() {
    return Object.keys(this)[0];
  }

  get tableAlias() {
    return this.tableAliasName ? this.tableAliasName : this.tableName;
  }

  get db() {
    return pool ? pool : null;
  }

}

module.exports = BaseModel;